public class Student
{
	private String name;
	private int grade;
	private boolean isGoodStudent;
	int amountLearnt;
	
	//------------------------------------------------
	
	public Student(String name, int grade)
	{
		this.name = name;
		this.grade = grade;
		this.isGoodStudent = true;
		this.amountLearnt = 0;
	}
	
	//------------------------------------------------
	/*
	public void setGrade(int inputGrade) 
	{
		this.grade = inputGrade;
	}
	
	public void setName(String inputName) 
	{
		this.name = inputName;
	}
	*/
	public void setIsGood(boolean isGood) 
	{
		this.isGoodStudent = isGood;
	}
	

	public void study()
	{
		if(this.isGoodStudent)
		{
			System.out.print(this.name + " is studying...");
		}
		else
		{
			System.out.print(this.name + " is NOT studying.");
		}
	}
	
	public void passOrFail()
	{
		if(this.grade>=60)
		{
			System.out.println(this.name + " passed the class!");
		}
		else
		{
			System.out.println(this.name + " failed the class.");
		}
	}
	
	public void learn(int amountStudied)
	{
		if(amountStudied>=0)
		{
			amountLearnt+=amountStudied;
		}
	}
	
	public String getName()
	{
       return this.name;
	}
	public int getGrade()
	{
       return this.grade;
	}
	public boolean getGoodStudent()
	{
       return this.isGoodStudent;
	}
	public int getAmountLearnt()
	{
       return this.amountLearnt;
	}


}