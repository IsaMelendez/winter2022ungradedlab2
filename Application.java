import java.util.Scanner;

public class Application
{
	public static void main(String[] args)
	{
		Student[] section3 = new Student[4];
		
		Scanner input = new Scanner(System.in);
		
		//--------------------------------
		
		System.out.println("NEW LAB: ");
		
		Student student1 = new Student("Isa", 89);
		
		student1.setIsGood(false);
		
		System.out.println(student1.getName());
		System.out.println(student1.getGrade());
		System.out.println(student1.getGoodStudent());
		System.out.println(student1.getAmountLearnt());
		
		
		
		//-----------------------------------
		/*
		System.out.println("OLD LAB: ");
		
		for(int i = 0; i<section3.length; i++)
		{
			section3[i] = new Student();
			
			System.out.print("What is the name of student number " + (i + 1) + "?");
			section3[i].setName(input.next());
			
			System.out.print("What grade does student number " + (i + 1) + " have?");
			section3[i].setGrade(input.nextInt());
			
			System.out.print("Is student number " + (i + 1) + " a good student?");
			String isGood = input.next();
			isGood = isGood.toLowerCase();
			
			while(!isGood.equals("yes") && !isGood.equals("no"))
			{
				System.out.println("Error: You must answer with yes or no.");
				isGood = input.next();
				isGood = isGood.toLowerCase();
			}
			
			if(isGood.equals("yes"))
			{
				section3[i].setIsGood(true);
			}
			else
			{
				section3[i].setIsGood(false);
			}
		}
		
		section3[0].study();
		section3[0].passOrFail();
		
		System.out.println("Input the amount studied: ");
		System.out.println(section3[0].amountLearnt);
		
		
		int amountStudied = input.nextInt();

		section3[3].learn(amountStudied);
		
		for(int i=0; i<section3.length; i++)
		{
			System.out.println("Student " + (i + 1) + " has learned this much: " + section3[i].amountLearnt);
		}
		
		section3[2].learn(3);
		System.out.println("Student 3 has learned this much: " + section3[3].amountLearnt);
		
		section3[2].learn(-10);
		System.out.println("Student 3 has learned this much: " + section3[3].amountLearnt);
		
		System.out.println("Info about student number 1: ");
		System.out.println(section3[0].getName());
		System.out.println(section3[0].getGrade());
		System.out.println(section3[0].getGoodStudent());
		System.out.println(section3[0].getAmountLearnt());
		*/
	}
}